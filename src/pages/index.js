import * as React from "react"
import { Link } from "gatsby"
import { StaticImage } from "gatsby-plugin-image"

import Layout from "../components/layout"
import Homepage from "../pages/homepage"

const IndexPage = () => (
  <Layout>
  <Homepage/>

  </Layout>
)

export default IndexPage
