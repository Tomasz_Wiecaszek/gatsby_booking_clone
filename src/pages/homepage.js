import React from "react";
import {homepage} from './homepage.module.css'
import Search from "../components/search";
import Tip from "../components/tip";
import Objects from "../components/objects";
import Countries from '../components/coutries'
import Types from '../components/types'

const Homepage=()=>{
    return(
      <div className={homepage} >
<Search/>
<Tip/>
<Objects />
<Countries/>
<Types />
      </div>
    )
}

export default Homepage;