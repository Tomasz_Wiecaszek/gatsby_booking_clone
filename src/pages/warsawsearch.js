import React from 'react'  
import Layout from '../components/layout';
import { navigate,Link,useStaticQuery, graphql } from 'gatsby'


import ItemPlace from '../components/itemplace'
const WarsawSearch=(props)=>{
console.log(props)
    const items = useStaticQuery(graphql`
    query {
        allMdx(filter: {frontmatter: {city: {eq: "Warszawa"}}}) {
          edges {
            node {
              frontmatter {
                name
                avatar
                description
              }
              slug
              body
            }
          }
        }
      }
      
  `)


    return(
<Layout>

{
    items.allMdx.edges.map((item)=>{

return(
    
<ItemPlace
 name={item.node.frontmatter.name}
 avatar={item.node.frontmatter.avatar}
 body={item.node.frontmatter.description}
 trace={item.node.slug}
 />
    


)

    })
}


</Layout>
    )
}

export default WarsawSearch;