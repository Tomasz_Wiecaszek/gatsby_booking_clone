import * as React from "react"
import {notfound} from './notfound.module.css'
import Layout from "../components/layout"
import Seo from "../components/seo"

const NotFoundPage = () => (
  <Layout>
  <Seo title="404: Not found" />
  <div className={notfound}>
  <h1>Nie znaleziono strony</h1> 
  <h3>Zdarza się, teraz spróbujmy to naprawić</h3> 
  </div>
  </Layout>
)

export default NotFoundPage
