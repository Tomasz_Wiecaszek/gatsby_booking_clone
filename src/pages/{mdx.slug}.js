import * as React from 'react'
import Layout from '../components/layout'
import { Link,useStaticQuery, graphql } from 'gatsby'
import { MDXRenderer } from 'gatsby-plugin-mdx'
import {placePage, placeTitle} from '../components/place.module.css'

const Place = ({data}) => {




  return (
    <Layout >
      <h1 className={placeTitle}>{data.mdx.frontmatter.name}</h1>
      <div className={placePage}>
      <MDXRenderer>
        {data.mdx.body}
      </MDXRenderer>
      </div>
    </Layout>
  )
}
export default Place;

export const query = graphql`
query ($id: String) {
  mdx(id: {eq: $id}) {
    frontmatter {
      name
    }
    body
  }
}
`