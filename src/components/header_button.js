import {header__button } from './layout.module.css'
import React from 'react'
const Header_button=({children})=>{
    return(
        <div className={header__button}>
<p>{children}</p>
        </div>
    )
}

export default Header_button;