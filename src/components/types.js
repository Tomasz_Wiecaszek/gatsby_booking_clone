import React from "react";
import {types, types_wrapper, hotel,apart,resort,villa,house} from './types.module.css'
import {place, numbers} from './countries.module.css'
import { navigate,Link,useStaticQuery, graphql } from 'gatsby'
const Types=()=>{

    const items = useStaticQuery(graphql`
    query {
        allMdx {
          edges {
            node {
              frontmatter {
                type
              }
            }
          }
        }
      }
  `)
  let hotelarr=[];
  let apartarr=[];
  let housearr=[];
  let villaarr=[];
  let resortarr=[];

  items.allMdx.edges.forEach((el)=>{
      
    if(el.node.frontmatter.type=="Hotel"){
        hotelarr.push(el)
    }
    else if(el.node.frontmatter.type=="Apartament"){
        apartarr.push(el)
    }
    else if(el.node.frontmatter.type=="Domek"){
        housearr.push(el)
    }
    else if(el.node.frontmatter.type=="Ośrodek wypoczynkowy"){
        resortarr.push(el)
    }
    else if(el.node.frontmatter.type=="Willa"){
        villaarr.push(el)
    }
      })
const numbersName=(number)=>{
if(number==1){
    return (number+" obiekt")
}
else if(number>1&&number<5){
    return (number+ ' obiekty')
}
else if(number>5){
    return (number+ ' obiektów')
}}



    return(
        <div className={types}>
            <h1>Szukaj według rodzaju obiektu</h1>
                <div className={types_wrapper}>
                    <div onClick={()=>{navigate('/hotelsearch');}}><div className={hotel}></div><p className={place}>Hotele</p><p className={numbers}>{numbersName(hotelarr.length)}</p></div>
                    <div onClick={()=>{navigate('/apartsearch');}}><div className={apart}></div><p className={place}>Apartamenty</p><p className={numbers}>{numbersName(apartarr.length)}</p></div>
                    <div onClick={()=>{navigate('/resortsearch');}}><div className={resort}></div><p className={place}>Ośrodki wypoczynkowe</p><p className={numbers}>{numbersName(resortarr.length)}</p></div>
                    <div onClick={()=>{navigate('/villasearch');}}><div className={villa}></div><p className={place}>Wille</p><p className={numbers}>{numbersName(villaarr.length)}</p></div>
                    <div onClick={()=>{navigate('/housesearch');}}><div className={house}></div><p className={place}>Domki</p><p className={numbers}>{numbersName(housearr.length)}</p></div>


                </div>
        



        </div>
    )
}

export default Types;