import React, { useEffect } from 'react';
import {objects,objects_big, object_medium, flagcss,warsawbg,gdakbg,pozbg,wrowbg,torbg} from './objects.module.css'
import { navigate,Link,useStaticQuery, graphql } from 'gatsby'
import { StaticImage } from 'gatsby-plugin-image'



const Objects=()=>{

    const items = useStaticQuery(graphql`
    query {
        allMdx {
          edges {
            node {
              frontmatter {
                city
                country
                name
              }
            }
          }
        }
      }
  `)
  let warsaw=[];
  let gda=[];
  let poz=[];
  let wro=[];
  let tor=[];

  items.allMdx.edges.forEach((el)=>{
if(el.node.frontmatter.city=="Warszawa"){
    warsaw.push(el)
}
else if(el.node.frontmatter.city=="Gdańsk"){
    gda.push(el)
}
else if(el.node.frontmatter.city=="Poznań"){
    poz.push(el)
}
else if(el.node.frontmatter.city=="Wrocław"){
    wro.push(el)
}
else if(el.node.frontmatter.city=="Toruń"){
    tor.push(el)
}
  })

  const flag=<div className={flagcss}><StaticImage alt="language_ico" src="../images/other/poland.png" width={25} height={18}/></div>;



    return(
        <div className={objects}>

           <div className={`${objects_big} ${warsawbg}`} onClick={()=>{navigate('/warsawsearch');}}><h1>Warszawa  {flag}</h1>  <h3>{warsaw.length} obiekty</h3></div>
            <div className={`${objects_big} ${gdakbg}`} onClick={()=>{navigate('/gdsearch')}}><h1>Gdańsk {flag}</h1>  <h3>{gda.length}  obiekty</h3></div>
            <div className={`${object_medium} ${pozbg}`} onClick={()=>{navigate('/pozsearch')}}><h1>Poznań {flag}</h1>  <h3>{poz.length}  obiekty</h3></div>
            <div className={`${object_medium} ${wrowbg}`} onClick={()=>{navigate('/wrosearch')}}><h1>Wrocław {flag}</h1>  <h3>{wro.length}  obiekty</h3></div>
            <div className={`${object_medium} ${torbg}`} onClick={()=>{navigate('/torsearch')}}><h1>Toruń {flag}</h1>  <h3>{tor.length} obiekty</h3></div>
        </div>
    )
}
export default Objects;