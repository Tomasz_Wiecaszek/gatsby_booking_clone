import React from 'react'
import { MDXRenderer } from 'gatsby-plugin-mdx'
import {itemplace} from './itemplace.module.css'
import { Link} from 'gatsby'


const ItemPlace=({name, avatar, body, trace})=>{
return(
    <div className={itemplace}>
<div><img src={avatar} alt="nopicture"/>    </div>

<div>
<p><Link to={`/${trace}`}>{name}</Link></p>
<p>

        {body}


</p></div>
</div>
)
}

export default ItemPlace;