import React from "react";

import {search, search_content, search_form,search_check,search_label,checkbox_wrapper} from './search.module.css'
import { Link,useStaticQuery, graphql } from 'gatsby'

const Search=()=>{


    const menu_icons = useStaticQuery(graphql`
    query{
      allFile(filter: {sourceInstanceName: {eq: "search_ico"}}) {
        edges {
          node {
            absolutePath
            name
            childImageSharp {
              fluid(maxHeight: 40, maxWidth: 40) {
                src
              }
            }
          }
        }
      }
    }
  `)
  

    return(
<div className={search}>
    <div className={search_content}>
<h1>Znajdź oferty hoteli, domów i wielu innych obiektów...</h1>
<h3>Od przytulnych domków wiejskich po modne apartamenty w mieście</h3>
<div className={search_form}>
    <div><img src={menu_icons.allFile.edges[0].node.childImageSharp.fluid.src} alt='logo'/><textarea placeholder="Dokąd się wybierasz?"></textarea></div>
    
    <div><img src={menu_icons.allFile.edges[1].node.childImageSharp.fluid.src} alt='logo'/>
    <p>Zameldowanie - Wymeldowanie</p>
    </div>
    <div><img src={menu_icons.allFile.edges[2].node.childImageSharp.fluid.src} alt='logo'/>
    <p>2 dorosłych &#183; 0 dzieci &#183; 1 pokój</p>
    </div>
    <button>Szukaj</button>
    <br/>
    
</div>
<div className={checkbox_wrapper}>
<input type='checkbox' className={search_check} id='check'/><label className={search_label} for='check'>Podróżuję służbowo</label>
</div>
    </div>
</div>

    )
}

export default Search;