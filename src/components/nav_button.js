import React from 'react'
import {nav__button} from './nav__button.module.css'
const NavButton=({ico,txt})=>{
    return(
        <div className={nav__button}>
            <img src={ico} alt="images_"/>
            <p>{txt}</p>
        </div>
    )
}

export default NavButton;