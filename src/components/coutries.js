import React from "react";
import {countries,countries_wrapper, place, numbers} from './countries.module.css'
import {balticbg,gdakbg,pozbg,wrowbg,torbg} from './objects.module.css'
import { navigate,Link,useStaticQuery, graphql } from 'gatsby'
const Countries=()=>{
    const items = useStaticQuery(graphql`
    query {
        allMdx {
          edges {
            node {
              frontmatter {
                city
                place
              }
            }
          }
        }
      }
  `)
  let baltic=[];
  let warsaw=[];
  let gda=[];
  let poz=[];
  let wro=[];
  let tor=[];

  items.allMdx.edges.forEach((el)=>{
      if(el.node.frontmatter.place=="Morze bałtyckie")
        baltic.push(el)
      
    if(el.node.frontmatter.city=="Warszawa"){
        warsaw.push(el)
    }
    else if(el.node.frontmatter.city=="Gdańsk"){
        gda.push(el)
    }
    else if(el.node.frontmatter.city=="Poznań"){
        poz.push(el)
    }
    else if(el.node.frontmatter.city=="Wrocław"){
        wro.push(el)
    }
    else if(el.node.frontmatter.city=="Toruń"){
        tor.push(el)
    }
      })
const numbersName=(number)=>{
if(number==1){
    return (number+" obiekt")
}
else if(number>1&&number<5){
    return (number+ ' obiekty')
}
else if(number>5){
    return (number+ ' obiektów')
}

}

    return(
        <div className={countries}>
            <h1>Polska - odkryj to miejsce</h1>
            <h3>Te popularne miejsca mają wiele do zaoferowania</h3>
            <div className={countries_wrapper}>
                <div onClick={()=>{navigate('/balticsearch')}}><div className={balticbg}></div><p className={place}>Morze bałtyckie</p><p className={numbers}>{numbersName(baltic.length)}</p></div>
                <div onClick={()=>{navigate('/gdsearch')}}><div className={gdakbg}></div><p className={place}>Gdańsk</p><p className={numbers}>{numbersName(gda.length)}</p></div>
                <div onClick={()=>{navigate('/torsearch')}}><div className={torbg}></div><p className={place}>Toruń</p><p className={numbers}>{numbersName(tor.length)}</p></div>
                <div onClick={()=>{navigate('/wrosearch')}}><div className={wrowbg}></div><p className={place}>Wrocław</p><p className={numbers}>{numbersName(wro.length)}</p></div>
                <div onClick={()=>{navigate('/pozsearch')}}><div className={pozbg}></div><p className={place}>Poznań</p><p className={numbers}>{numbersName(poz.length)}</p></div>

            </div>
        </div>
    )
}
export default Countries;