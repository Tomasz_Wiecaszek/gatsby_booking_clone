import React from "react";
import {footer, footer_wrapper, footer_allright,footer_company} from './footer.module.css'
import {Link} from 'gatsby'

const Footer=()=>{
    return(
        <div className={footer}>
            <div className={footer_wrapper}>
<Link to='/about'><p>O booking.com</p></Link>
<Link to='/owh'><p>Ogólne Warunki Handlowe</p></Link>
<Link to='/policy'><p>Oświadczenie o ochronie prywatności i plikach cookies</p></Link>
<Link to='/help'><p>Atrakcje – centrum pomocy</p></Link>

            </div>
            <div><p className={footer_allright}>Prawa autorskie © 1996–2021 Booking.com™. Wszystkie prawa zastrzeżone.</p></div>
            <div><p className={footer_company}>Booking.com jest częścią Booking Holdings Inc. – światowego lidera w internetowej branży turystycznej.</p></div>


        </div>
    )
}
export default Footer;