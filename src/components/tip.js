import React from 'react';
import { Link,useStaticQuery, graphql } from 'gatsby'
import {tip} from './tip.module.css'

const Tip=()=>{

    const tip_ico = useStaticQuery(graphql`
    query{
      allFile(filter: {sourceInstanceName: {eq: "tip_ico"}}) {
        edges {
          node {
            absolutePath
            name
            childImageSharp {
              fluid(maxHeight: 40, maxWidth: 40) {
                src
              }
            }
          }
        }
      }
    }
  `)

    return(
        <div className={tip}><img src={tip_ico.allFile.edges[0].node.childImageSharp.fluid.src}/><p>Otrzymaj porady, których potrzebujesz. Sprawdź najnowsze ograniczenia związane z COVID-19, zanim wyruszysz w podróż. <br/><Link to='/more'>Dowiedz się więcej</Link></p> </div>
    )
}

export default Tip;