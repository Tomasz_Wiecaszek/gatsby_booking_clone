import * as React from "react"
import { Link,useStaticQuery, graphql } from 'gatsby'
import PropTypes from "prop-types"
import { header,header__logo,header__buttons,header__shareButton, header__logButton,header__navigation } from './layout.module.css'
import { StaticImage } from 'gatsby-plugin-image'
import Header_button from "./header_button"
import NavButton from "./nav_button"


const Header = () => {

  const menu_icons = useStaticQuery(graphql`
  query{
    allFile(filter: {sourceInstanceName: {eq: "menu_ico"}}) {
      edges {
        node {
          absolutePath
          name
          childImageSharp {
            fluid(maxHeight: 40, maxWidth: 40) {
              src
            }
          }
        }
      }
    }
  }
`)




  return(
  <header className={header}>
    <div >
      <div className={header__logo}>
        <div><Link to='/'>
      <StaticImage alt="Booking logo" src="../images/images/logo.png" width={140} height={20} layout='fixed'/></Link>
        </div>
      <div className={` ${header__buttons}`}>
      <Header_button>PLN</Header_button>
      <Header_button><StaticImage alt="language_ico" src="../images/images/pl.png" width={25} height={25}/></Header_button>
      <Header_button><StaticImage alt="help_ico" src="../images/images/ask.png" width={25} height={25}/></Header_button>
      <button className={header__shareButton}><Link to='/share' style={{ color: 'inherit', textDecoration: 'inherit'}}>Udostępnij obiekt</Link></button>
      <button className={header__logButton}><Link to='/login' style={{ color: 'inherit', textDecoration: 'inherit'}}>Zarejestruj się</Link></button>
      <button className={header__logButton}><Link to='/registration' style={{ color: 'inherit', textDecoration: 'inherit'}}>Zaloguj się</Link></button>
      </div>

      </div>
    </div>
    <div className={header__navigation}>
      {
menu_icons.allFile.edges.map((index)=>{return(
<NavButton ico={index.node.childImageSharp.fluid.src} txt={index.node.name}/>
)})

      }


    </div>
  </header>)
}

Header.propTypes = {
  siteTitle: PropTypes.string,
}

Header.defaultProps = {
  siteTitle: ``,
}

export default Header
