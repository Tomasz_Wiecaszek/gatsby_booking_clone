module.exports = {
  siteMetadata: {
    title: `Booking Clone`,
    description: `This site was creaced only for education.`,
    author: `Tomasz Więcaszek`,
    siteUrl: `https://gatsbystarterdefaultsource.gatsbyjs.io/`,
  },
  plugins: [
    "gatsby-plugin-gatsby-cloud",
    "gatsby-plugin-image",
    "gatsby-plugin-sharp",
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `images`,
        path: `${__dirname}/src/images/images`,
      },
    },
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `menu_ico`,
        path: `${__dirname}/src/images/menu_ico`,
      },
    },
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `search_ico`,
        path: `${__dirname}/src/images/search_ico`,
      },
    },
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `tip_ico`,
        path: `${__dirname}/src/images/tip_ico`,
      },
    },
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `objects`,
        path: `${__dirname}/objects`,
      },
    },
    `gatsby-transformer-sharp`,
    `gatsby-plugin-sharp`,
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: `gatsby-starter-default`,
        short_name: `starter`,
        start_url: `/`,
        background_color: `#663399`,
        theme_color: `#663399`,
        display: `minimal-ui`,
        icon: `src/images/images/gatsby-icon.png`, // This path is relative to the root of the site.
      },
    },
    `gatsby-plugin-gatsby-cloud`,
    "gatsby-plugin-mdx",
    `gatsby-transformer-remark`,
    // this (optional) plugin enables Progressive Web App + Offline functionality
    // To learn more, visit: https://gatsby.dev/offline
    // `gatsby-plugin-offline`,
  ],
}
